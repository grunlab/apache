[![pipeline status](https://gitlab.com/grunlab/apache/badges/main/pipeline.svg)](https://gitlab.com/grunlab/apache/-/commits/main)

# GrunLab Apache

Apache non-root container image.

Docs: https://docs.grunlab.net/images/apache.md

Base image: [grunlab/base-image/debian:12][base-image]

Format: docker

Supported architecture(s):
- arm64

GrunLab project(s) using this image:
- [grunlab/dl][dl]
- [grunlab/mkdocs][mkdocs]

[base-image]: <https://gitlab.com/grunlab/base-image>
[dl]: <https://gitlab.com/grunlab/dl>
[mkdocs]: <https://gitlab.com/grunlab/mkdocs>